FROM openjdk:8
ADD target/*.jar ecom-1.0.jar
EXPOSE 8080
workdir /workspace
ENTRYPOINT ["java","-jar","ecom-1.0.jar"]

