#!/bin/sh
  
echo "Got merge request $CI_COMMIT_REF_NAME for branch $CI_PROJECT_NAME!!"

# Install ShiftLeft
curl https://www.shiftleft.io/download/sl-latest-linux-x64.tar.gz > /tmp/sl.tar.gz && tar -C /usr/local/bin -xzf /tmp/sl.tar.gz
sl analyze --version-id "$CI_COMMIT_SHA" --tag branch="$CI_COMMIT_REF_NAME" --app "$CI_PROJECT_NAME" --wait target/<war-or-jar-file-name>
# Run build rules  
URL="https://www.shiftleft.io/violationlist/$CI_PROJECT_NAME?apps=$CI_PROJECT_NAME&isApp=1"
BUILDRULECHECK=$(sl check-analysis --app "$CI_PROJECT_NAME" --branch "$CI_COMMIT_REF_NAME")

if [ -n "$BUILDRULECHECK" ]; then
    MR_COMMENT="Build rule failed, click here for vulnerability list - $URL" 
    curl -XPOST "https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
      -H "PRIVATE-TOKEN: $PRIVATE-TOKEN" \
      -H "Content-Type: application/x-www-form-urlencoded" \
      --data-urlencode "body=$MR_COMMENT"
    exit 1
else
    MR_COMMENT="Build rule succeeded, click here for vulnerability list - $URL" 
    curl -XPOST "https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
      -H "PRIVATE-TOKEN: $PRIVATE-TOKEN" \
      -H "Content-Type: application/x-www-form-urlencoded" \
      --data-urlencode "body=$MR_COMMENT"
    exit 0
fi
